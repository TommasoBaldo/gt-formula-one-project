
### Game Theory Project ###
The content of this repository is related to a Game Theory project developed by Tommaso Baldo and Andrea Frasson for academic purposes.
It contains the python notebook used in order to simulate over 5000 race times and compute the payoffs needed in our examples.
